# Exec Each

Read in lines from a file or stdin. Cut each line into columns. Use string interpolation to build a series of commands to print or execute from the columnar input.

## USAGE

    usage: execEach [-h] [-v] [--execute] [-d DELIM] [-b] [-i INPUT] [-o OUTPUT]
                    [--output-delim OUTPUT_DELIM] [--empty-lines]
                    command [command ...]
    
    Read in lines from a file or stdin. Cut each line into columns. Use string
    interpolation to build a series of commands to print or execute from the
    columnar input.
    
    positional arguments:
      command               command to interpolate columns using python string
                            formatting (e.g., 'echo {1} {2} {3}')
    
    optional arguments:
      -h, --help            show this help message and exit
      -v, --verbose         be verbose
      --execute             treat the output as a series of shell commands to
                            execute
      -d DELIM, --delim DELIM
                            regular expression delimeter to split each line
                            (default: '\s+')
      -b, --buffer          buffer the input into a string first, then split the
                            string
      -i INPUT, --input INPUT
                            input file (default <stdin>)
      -o OUTPUT, --output OUTPUT
                            output file for text transformation (default <stdout>)
      --output-delim OUTPUT_DELIM
                            string to use when joining ranges of columns for
                            output (default: ' ')
      --empty-lines         output empty lines (default: False)
    
## EXAMPLE

    $ ls -la
    total 16
    drwxr-xr-x   6 jordan  staff   204B Jan  7 10:16 .
    drwxr-xr-x  83 jordan  staff   2.8K Jan  7 10:13 ..
    drwxr-xr-x  13 jordan  staff   442B Jan  7 10:16 .git
    -rw-r--r--   1 jordan  staff     0B Jan  7 10:16 README.md
    lrwxr-xr-x   1 jordan  staff    11B Jan  7 10:13 execEach -> execEach.py
    -rwxr-xr-x   1 jordan  staff   1.9K Jan  7 10:15 execEach.py

    $ ls -la | execEach.py echo {9}
    echo .
    echo ..
    echo .git
    echo README.md
    echo execEach
    echo execEach.py

    $ ls -la | execEach.py echo {9} --execute
    .
    ..
    .git
    README.md
    execEach
    execEach.py

## FORMATTING

This tool uses Python string formatting for command interpolation. While reading
input, this tool splits each line of text into columns based on the `--delim` 
argument. These columns are available to the command interpolation using the 
bracket syntax: 

    {0} - The entire line
    {1} - The first column
    {2} - The second column
    etc.

Therefore, if you use the command:
    
    execEach echo {1} {5}

The tool will output the first and fifth columns, delineated by `--delim`


## RATIONALE

I do a lot of command line text column interpolation using ls, find, and awk.
Here's what I hate typing:

    ls -la | grep *.orig | awk '{print $9}' | while read f; do rm $f; done

Another alternative is to use the `find` command. The above command is
equivalent to:

    find . -name *.orig -exec rm {} \;

But, I don't like using either of these approaches because: 

1. the awk approach requires a complex while loop
2. the awk approach requires the input to come from stdin, unless you cat and pipe a file
3. the find approach will only work with files/directories, and not arbitrary data
4. each approach requires you to add `echo` to the command if you want to see the command before its executed (which I generally do for safety)

To me, it's much easier to be able to run:

    ls -la | grep *.orig | execEach rm {9}

But, I can even read items from a file instead of stdin:

    execEach -i test.txt rm {9}

Or I can still chain commands to clean up files reported by git status:

    git status | grep *.orig | execEach rm {1}

And because we print out the command interpolation by default, if I accidentaly
run any of these commands, like:
    
    ls -la /tmp | execEach rm -rf {0}

It won't actually delete all of the files, just print out the commands that will
be run. I can then check the output to make sure the commands are correct. Then
all I have to do is add the `--execute` flag to execute the commands that are
generated:

    ls -la /tmp | execEach rm -rf {0} --execute

But that's not all. Because we interpolate and print the output by default we
can use this tool just for text transformation if we want to. Here's an example
that reads columns from a csv, reorders them, and writes the results to a new
csv file: 

    execEach -i test.csv -o test.new.csv -d , {2},{1},{3}

NOTE: This does not take escaped delimeters or newlines in the csv into account.
This is just an example to illustrate the flexibility of the tool, not its 
compatibility with dealing with CSV. 


## TODO

I think it would be interesting to use Jinja2 style text filters with this approach.
Then, interesting text transformations could be had, like:

    ls -la | execEach {8-10|date('%y%m%d')}


## LICENSE

    Copyright (c) 2013, Jordan Sherer <jordan@widefido.com>
    All rights reserved.
    
    Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
    
    Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
    Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
