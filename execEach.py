#!/usr/bin/env python

"""
execEach - A utility for easily manipulating columnar data on the command line.

Read in lines from a file or stdin. Cut each line into columns. Use string
formatting and interpolation to output lines of text from the columnar input. 

Optionally, sort the input based columnar ordering (1 desc, 2 asc)
Optionally, treat each of those lines as a shell command to be executed.
Optionally, compute summary statistics on the numerical data in a column.

---

Copyright (c) 2013, Jordan Sherer <jordan@widefido.com>
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""

import argparse
import io
import math
import re
import subprocess
import sys

if hasattr(sys.stdin, "buffer"):
    sys.stdin = io.TextIOWrapper(sys.stdin.buffer, encoding='utf8')
    sys.stdout = io.TextIOWrapper(sys.stdout.buffer, encoding='utf8')


def execute(command, verbose=False):
    """
    Execute a shell command.

    """
    if verbose:
        sys.stderr.write("executing `{0}`...\n".format(command))
        sys.stderr.flush()
    subprocess.call(command, shell=True)


def try_int(string, default=None):
    try:
        return int(string)
    except:
        return default


def try_float(string, default=None):
    try:
        return float(string)
    except:
        return default


def try_re(string, default=None, **kwargs):
    try:
        return re.compile(string, **kwargs)
    except:
        return default


def natural_sort_key(s):
    # natural sorting based on: http://stackoverflow.com/questions/4836710/
    #  does-python-have-a-built-in-function-for-string-natural-sort
    return [int(text) if text.isdigit() else text.lower()
            for text in re.split('([0-9]+)', s)]   


def process(input_file, command,
        delimeter="\s+",
        output_delimeter=" ",
        dry=False,
        buffer_input=False,
        sort_columns=None,
        summarize_columns=None,
        summarize_output_file=None,
        output_file=None,
        output_empty=False,
        verbose=False):
    """
    Process each line in the input_file. Split each line into columns and 
    print or execute the interpolated output.

    """
    command = " ".join(command)
    segs = re.findall("\{(\d+)\}", command) 

    if segs:
        max_seg = max(try_int(value) for value in segs)
    else:
        max_seg = 0

    ranges = re.finditer("\{((-?\d+)[-](-?\d+)?)\}", command)
    if ranges:
        ranges = [(m.group(1), try_int(m.group(2)), try_int(m.group(3))) for m in ranges]
        for k, a, b in ranges:
            max_seg = max(max_seg, max(a, b))

    regexes = re.finditer("\{(\d+)[:]([^}]+?)(?:[:]([^}]*))?\}", command)
    if regexes:
        regexes = [(m.group(0), try_int(m.group(1)), try_re(m.group(2)), m.group(3)) for m in regexes]
        for m, a, b, c in regexes:
            max_seg = max(max_seg, a)

    split = re.compile(delimeter, re.I | re.U)

    if buffer_input:
        contents = input_file.read().rstrip()
        input_file = contents.split("\n")

    
    def compute_columns():
        for line in input_file:
            line = line.strip()
            columns = [line]
            columns.extend(split.split(line))
            yield columns

    
    # create a generator for the normal flow
    input_columns = compute_columns()

    
    # then if we are sorting. create a separate generator after sorting
    if sort_columns:
        column_re = re.compile("(\d+)(?:\s*(asc|desc))?")
        sort_columns = [
            (int(column), -1 if direction == "desc" else 1)
                for column, direction in column_re.findall(sort_columns)]

        # because we are sorting, we need to consume the input_columns 
        # generator to build an internal list so we can apply sorting.
        input_columns = list(input_columns)
        
        # python has a stable sort mechanism, meaning to do a sql-like order_by
        # we can apply sorting in reverse.
        # i.e., 1 asc, 2 desc would sort 2 in reverse, then sort 1.
        for column, direction in reversed(sort_columns):
            # retrieve a column from the columns list
            column_key = lambda columns: \
                "" if column >= len(columns) else\
                   natural_sort_key(columns[column])

            input_columns.sort(key=column_key, reverse=(direction < 0))
    
    summaries = {}
    if summarize_columns:
        column_re = re.compile("(\d+)")
        summarize_columns = [
            int(column) for column in column_re.findall(summarize_columns)]

    # process the columns
    for columns in input_columns:
        process_line(command,
            columns,
            output_file=output_file,
            dry=dry,
            verbose=verbose,
            output_empty=output_empty,
            output_delimeter=output_delimeter,
            max_seg=max_seg,
            ranges=ranges, 
            regexes=regexes)

        if summarize_columns:
            process_incremental_summary(summaries, summarize_columns, columns)

    # output summaries
    for column in sorted(summaries.keys()):
        summary = summaries.get(column)
        if not summary:
            pass
        summarize_output_file.write("\n")
        summarize_output_file.write("Summary Column {0}:\n".format(column))
        summarize_output_file.write("- min: {0}\n".format(summary.get("min")))
        summarize_output_file.write("- max: {0}\n".format(summary.get("max")))
        summarize_output_file.write("- avg: {0}\n".format(summary.get("avg")))
        summarize_output_file.write("- var: {0}\n".format(summary.get("var")))
        summarize_output_file.write("- std: {0}\n".format(summary.get("std")))
        summarize_output_file.write("-   n: {0}\n".format(summary.get("n")))
        summarize_output_file.write("\n")


def process_incremental_summary(summaries, summarize_columns, columns):
    column_count = len(columns)

    for column in summarize_columns:
        if column >= column_count:
            pass
        incremental_summary = process_incremental_column_summary(summaries.get(column), column, columns[column])
        if incremental_summary:
            summaries[column] = incremental_summary


def process_incremental_column_summary(summary, column, data):
    observation = try_float(data)
    if observation is None:
        return

    if not summary:
        return {
            "min":observation, 
            "max":observation,
            "avg":observation,
            "var":0,
            "std":0,
            "n":1
        }

    previous_min = summary["min"]
    previous_max = summary["max"]
    previous_avg = summary["avg"]
    previous_var = summary["var"]
    previous_n = summary["n"]

    new_min = min(observation, previous_min)
    new_max = max(observation, previous_max)
    new_n = previous_n + 1
    new_avg = previous_avg + ((observation - previous_avg)/new_n)
    new_var = previous_var + ((observation - previous_avg) * (observation - new_avg))
    new_std = math.sqrt(new_var / (new_n - 1))

    return {
        "min":new_min, 
        "max":new_max,
        "avg":new_avg,
        "var":new_var,
        "std":new_std,
        "n":new_n
    }


def process_line(command, columns, output_file=None, dry=True, verbose=True, output_empty=False, output_delimeter=" ", max_seg=0, ranges=tuple(), regexes=tuple()):
    # interpolate standard columns (make sure we have enough columns for
    # the requested max column)
    if len(columns) <= max_seg:
        columns.extend([""] * (max_seg - len(columns) + 1))

    named_columns = {}

    # interpolate ranges (named columns)
    if ranges:
        for k, a, b in ranges:
            direction = -1 if b is not None and b < a else 1

            if direction > 0 and b is not None:
                b = b + 1
            else:
                a = a + 1

            if a == 0:
                a = None
            if b == 0:
                b = None
            
            r = slice(a, b, direction)
            named_columns[k] = output_delimeter.join(columns[r])

    # interpolate regexes
    if regexes:
        count = 0
        for matched_text, matched_column, regex, sub in regexes:
            column_text = columns[matched_column]
            if sub is None:
                column_matches = regex.findall(column_text)
                regex_text = output_delimeter.join(column_matches)
                key = "r{0}".format(count)
                named_columns[key] = regex_text
                command = command.replace(matched_text, "{%s}" % key)
                count += 1
            elif column_text:
                column_text = re.sub(regex, sub, column_text)
                key = "r{0}".format(count)
                named_columns[key] = column_text
                command = command.replace(matched_text, "{%s}" % key)
                count += 1

    output = command.format(*columns, **named_columns)

    # skip empty lines if output_empty = False and the line contains no text
    if not output_empty and not output.strip().strip(output_delimeter):
        return

    if dry:
        if output_file:
            output_file.write(output)
            output_file.write("\n")
    else:
        execute(output, verbose=verbose)



def main():
    parser = argparse.ArgumentParser(description="Read in lines from a file "
        "or stdin. Cut each line into columns. Use string interpolation to "
        "build a series of commands to print or execute from the columnar "
        "input. Optionally interpolate a range of columns {1-3}, or a regex "
        "text extraction of a specific column {1:(\d+)}, or a regex text "
        "substitution of a specific column {1:([a-z]+):test-\\1)}.")
        #,formatter_class=argparse.RawTextHelpFormatter)

    parser.add_argument("command", nargs="+", help="command to interpolate "
            "columns using python string formatting (e.g., 'echo {1} {1-2} {2:<regex>} {3:<regex>:<replacement>}')")

    parser.add_argument("-v", "--verbose", action="store_true", default=False,
        help="be verbose")

    parser.add_argument("--execute", action="store_true", default=False,
        help="treat the output as a series of shell commands to execute")
    
    parser.add_argument("-d", "--delim",
        default="\s+",
        help="regular expression delimeter to split each line into columns (default: '\s+')")

    parser.add_argument("-b", "--buffer", action="store_true", default=False, 
        help="buffer the input into a string first, then split the string")

    parser.add_argument("-s", "--sort", metavar="COLUMNS", help="sort the output based on the "
        "columnar data. this is a comma separated list of column numbers "
        "to sort by in order (e.g., 1 desc, 2 asc, 3). "
        "note: this will force the input to be buffered.")

    parser.add_argument("-i", "--input",
        default=sys.stdin,
        type=argparse.FileType("r"),
        help="input file (default <stdin>)")

    parser.add_argument("-o", "--output",
        default=sys.stdout,
        type=argparse.FileType("w"),
        help="output file for text transformation (default <stdout>)")

    parser.add_argument("--output-delim",
        default=" ",
        help="string to use when joining ranges of columns for output (default: ' ')")

    parser.add_argument("--empty-lines",
        default=False,
        action="store_true",
        help="output empty lines (default: False)")

    parser.add_argument("-z", "--summarize", metavar="COLUMNS", default=None,
        help="summarize a set of columns. this is a comma separated list of "
        "column numbers to use in the summary. output will include: "
        "min, max, average, and stddev. "
        "note: this will force the input to be buffered.")

    parser.add_argument("--summarize-output",
        default=sys.stderr,
        type=argparse.FileType("w"),
        help="output file for column summaries (default <stderr>)")
    
    args = parser.parse_args()

    args.dry = not args.execute
    
    process(args.input, args.command,
        delimeter=args.delim,
        output_delimeter=args.output_delim,
        dry=args.dry,
        buffer_input=args.buffer,
        sort_columns=args.sort,
        summarize_columns=args.summarize,
        summarize_output_file=args.summarize_output,
        output_file=args.output,
        verbose=args.verbose, 
        output_empty=args.empty_lines)


if __name__ == "__main__":
    main()
